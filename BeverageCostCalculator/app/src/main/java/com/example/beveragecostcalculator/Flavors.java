package com.example.beveragecostcalculator;

public enum Flavors {
    //possible types of flavors and its costs as a enum class
    VANILLA(0.25),
    CHOCOLATE(0.75),
    NONE(0.00),
    LEMON(0.25),
    MINT(0.50);

    private Double cost;

    Flavors(Double cost){
        this.cost = cost;
    }

    //method to get the cost of a specific flavor
    public Double getCost(){
        return cost;
    }
}
