package com.example.beveragecostcalculator;

import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private String[] Provinces = {"Ontario", "Quebec", "Nova Scotia", "New Brunswick", "Manitoba",
    "British Columbia", "Prince Edward Island", "Saskatchewan", "Alberta", "Newfoundland and Labrador"};
    private String[] sizes = {"small", "medium", "large"};
    private EditText edName;
    private RadioButton rdbTea, rdbCoffee, rdbNone, rdbMinChoc, rdbVanLem;
    private CheckBox chekMilk, chekSugar;
    private String beverageChoice = "";
    private String flavor = "";
    private String size = "";
    private Order order;
    private Button btOrder;
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //making the reference of each control in layout to the code by their IDs
        btOrder = (Button)findViewById(R.id.btnOrder);
        edName = (EditText)findViewById(R.id.etxtName);
        rdbTea = (RadioButton)findViewById(R.id.rbTea);
        rdbCoffee = (RadioButton)findViewById(R.id.rbCoffee);
        rdbNone = (RadioButton)findViewById(R.id.rbNone);
        rdbMinChoc = (RadioButton)findViewById(R.id.rbMintChocolate);
        rdbVanLem = (RadioButton)findViewById(R.id.rbVanillaLemon);
        chekMilk = (CheckBox)findViewById(R.id.chkMilk);
        chekSugar = (CheckBox)findViewById(R.id.chkSugar);
        spinner = (Spinner)findViewById(R.id.spnSize);
        final AutoCompleteTextView autoProvince = (AutoCompleteTextView)findViewById(R.id.autoListProvince);

        //populating the spinner object from sizes array using adapter to fetch the data
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, sizes);
        //setting the adapater as a drop down view
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //setting the adapater of spinner with the adapter that contain the data
        spinner.setAdapter(adapter);

        //populating the adaptor from an array with all the provinces
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, Provinces);
        //setting the adapter of AutoCompleteTextView control to be able to fetch the suggestions
        autoProvince.setAdapter(adapter1);

        //every time that the user select a option from the drop down list, size field is populated
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                size = sizes[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //action of click on Order button
        btOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String errorsMsg = "Please complete ";
                Boolean valid = true;

                //cr4eating Order object
                order = new Order();

                order.setCustomerName(edName.getText().toString());
                order.setProvince(autoProvince.getText().toString());

                //checking for required fields
                if (edName.length() < 2){
                    valid = false;
                    errorsMsg += "'name' ";
                }
                if (beverageChoice.equals("")) {
                    valid = false;
                    errorsMsg += "'beverage type' ";
                }
                if (size.equals("")){
                    valid = false;
                    errorsMsg += "'beverage size' ";
                }
                if (autoProvince.length() == 0){
                    valid = false;
                    errorsMsg += "'province' ";
                }

                //after choose a beverage, check the flavor and size using their specific functions
                if (beverageChoice.equals("coffee")){
                    order.setBeverageType("coffee");
                    checkSize();
                    boolean validFlavor = checkCoffeeFlavor();
                    if (!validFlavor){
                        valid = false;
                        errorsMsg += "'flavor' ";
                    }
                }else {
                    order.setBeverageType("tea");
                    checkSize();
                    boolean validFlavor = checkTeaFlavor();
                    if (!validFlavor){
                        valid = false;
                        errorsMsg += "'flavor' ";
                    }
                }

                //checking if any of checks button are checked -> it is possible do not check an option
                if(chekSugar.isChecked() && chekMilk.isChecked()){
                    order.setAddtionals("with sugar and milk");
                    order.setSubtotal(2.25);
                }
                else {
                    if(chekSugar.isChecked()){
                        order.setAddtionals("with sugar");
                        order.setSubtotal(1.0);
                    }
                    if(chekMilk.isChecked()){
                        order.setAddtionals("with milk");
                        order.setSubtotal(1.25);
                    }
                }

                //if valid is true, so just response for the user his/her order
                //if valid is false, send a message with the error(s)
                if (valid) {
                    String finalOrder = "For " + order.getCustomerName() + " from " + order.getProvince() + ", a "
                            + order.getBeverageSize().toString().toLowerCase() + " " + order.getBeverageType() + ", " +
                            order.getAddtionals() + ", " + order.getFlavor() + ", cost: "
                            + order.calculateTotal();
                    Toast.makeText(getApplicationContext(), finalOrder ,Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), errorsMsg ,Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    //method to check the size of user's choice and setting using Order's object method
    private void checkSize(){
        switch (size) {
            case "small":
                order.setBeverageSize(Size.SMALL);
                order.setSubtotal(Size.SMALL.getCost());
                break;
            case "medium":
                order.setBeverageSize(Size.MEDIUM);
                order.setSubtotal(Size.MEDIUM.getCost());
                break;
            case "large":
                order.setBeverageSize(Size.LARGE);
                order.setSubtotal(Size.LARGE.getCost());
                break;
        }
    }

    //function to check the flavors of user's coffee choice and setting using Order's object method
    //if there is no radio button checked, then return false to response a message for the user
    private boolean checkCoffeeFlavor(){
        if(rdbVanLem.isChecked()){
            order.setFlavor(Flavors.VANILLA);
            order.setSubtotal(Flavors.VANILLA.getCost());
            return true;
        }
        else if(rdbMinChoc.isChecked()){
            order.setFlavor(Flavors.CHOCOLATE);
            order.setSubtotal(Flavors.CHOCOLATE.getCost());
            return true;
        } else if(rdbNone.isChecked()){
            order.setFlavor(Flavors.NONE);
            return true;
        } else {
            return false;
        }
    }

    //function to check the flavors of user's coffee choice and setting using Order's object method
    //if there is no radio button checked, then return false to response a message for the user
    private boolean checkTeaFlavor(){
        if(rdbMinChoc.isChecked()){
            order.setFlavor(Flavors.MINT);
            order.setSubtotal(Flavors.MINT.getCost());
            return true;
        } else if(rdbVanLem.isChecked()){
            order.setFlavor(Flavors.LEMON);
            order.setSubtotal(Flavors.LEMON.getCost());
            return true;
        } else if(rdbNone.isChecked()){
            order.setFlavor(Flavors.NONE);
            return true;
        } else {
            return false;
        }
    }

    //method that handle the event of beverage types radio buttons
    //depending on the choice, it will set to visible different kind of flavors
    public void onRadioBeverageClicked(View view){
        if(rdbCoffee.isChecked()){
            beverageChoice = "coffee";
            rdbVanLem.setText("Vanilla");
            rdbMinChoc.setText("Chocolate");
            rdbVanLem.setVisibility(View.VISIBLE);
            rdbMinChoc.setVisibility(View.VISIBLE);
            rdbNone.setVisibility(View.VISIBLE);
        }
        if(rdbTea.isChecked()){
            beverageChoice = "tea";
            rdbVanLem.setText("Lemon");
            rdbMinChoc.setText("Mint");
            rdbVanLem.setVisibility(View.VISIBLE);
            rdbMinChoc.setVisibility(View.VISIBLE);
            rdbNone.setVisibility(View.VISIBLE);
        }
    }
}
