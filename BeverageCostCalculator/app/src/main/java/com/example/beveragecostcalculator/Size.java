package com.example.beveragecostcalculator;

public enum Size {
    //possible sizes of beverages and its costs as a enum class
    SMALL(1.50),
    MEDIUM(2.50),
    LARGE(3.25);

    private Double cost;

    Size(Double cost){
        this.cost = cost;
    }

    //method to get the cost of a specific beverage size
    public Double getCost(){
        return cost;
    }
}
