package com.example.beveragecostcalculator;

import java.text.NumberFormat;

public class Order {
    //declaring the properties of the class
    private String customerName;
    private String beverageType;
    private Size beverageSize;
    private Flavors flavor;
    private String addtionals;
    private String province;
    private Double subtotal;

    //constructor method of the class that has the same name of the class, without return type, called automatic when the object/instance is created
    //initializing the subtotal value
    public Order() {
        subtotal = 0.0;
        addtionals = "";
        customerName = "";
        beverageType = "";
        province = "";
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getBeverageType() {
        return beverageType;
    }

    public void setBeverageType(String beverageType) {
        this.beverageType = beverageType;
    }

    public Size getBeverageSize() {
        return beverageSize;
    }

    public void setBeverageSize(Size beverageSize) {
        this.beverageSize = beverageSize;
    }

    public String getFlavor() {
        if(flavor.equals(Flavors.NONE)) {
            return "no flavoring";
        }
        return flavor.toString().toLowerCase();
    }

    public void setFlavor(Flavors flavor) {
        this.flavor = flavor;
    }

    public String getAddtionals() {
        if (addtionals.equals(""))
            return "with no addtionals";
        else
            return addtionals;
    }

    public void setAddtionals(String addtionals) {
        this.addtionals = addtionals;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal += subtotal;
    }

    //member method of the class to calculate the total cost of an order
    String calculateTotal() {
        //setting the tax as final to no be possible to edit it (fixed value)
        final Double tax = 0.13;

        Double total = getSubtotal() + (getSubtotal() * tax);

        //returning the total amount of the order in a currency format rounded to two fractional digits
        return NumberFormat.getCurrencyInstance().format(total);
    }

}
