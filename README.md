# Beverage_Calculator

Beverage Cost Calculator - App for android developed in Java

Beverage Cost Calculator

1 Overview: This document specifies the functional, non-functional, and deliverable requirements for the Beverage Cost Calculator application development assignment. 

2 Scope:  This assignment is designed to allow you to master the implementation of the standard GUI elements that you will encounter on the current generation of mobile phones. You are tasked to implement a user interface that uses the UI elements outlined in this document and to store their attributes and states in the Preferences Store that exists for your Mobile OS of choice. 

3. Functional Requirements:  The following functional requirements must be implemented to complete the assignment.

3.1 View the App Title in a Label (TextView): The user must be able to view a label object centered horizontally at the top of the view space that says ‘The Beverage’

3.2 Enter Text in a Textbox: The user must be able to enter and view a customer’s name in a text box located on the view space.

3.3 Check a Radio Button: The user must be able to view and manipulate the type of beverage ‘Coffee’ or ‘Tea’.

3.4 Check a Checkbox:  The user must be able to view and manipulate the addition of milk and/ or sugar.

3.5 Manipulate a Spinner Control: The user must be able to view and manipulate a Spinner Dropdown list to select the size of beverage (small, medium, large). 

3.6 Manipulate a Radio Button Group (invisible): The user must be able to select any added flavorings.

•	For coffee this could be none, vanilla or chocolate.

•	For tea this could be none, lemon or mint.



3.7 Manipulate AutoCompleteTextView: The user must be able to select the name of the province by typing the first two letters of the province. 


3.8 Manipulate Button(Toast)(Validate here otherwise show an error message): The program will compute the amount of money that the customer owes for the beverage and display a single line of output as a toast describing the beverage and the cost, when a button is clicked.
 e.g.: For Peter from Ontario, a medium tea, with milk, no flavoring, cost: $2.83.


3.9 The cost: Cost depends on the size of the beverage and any additional flavorings, milk and/ or sugar. The costs are as follows:

a. Size of beverage:

    i.  Small: $1.50
    
    ii. Medium: $2.50
    
    iii. Large: $3.25

b. Milk and/or Sugar

    i.  Milk: $1.25
    
    ii. Sugar: $1.00

c. Coffee flavorings:
    
    i.  Vanilla: $0.25
    
    ii. Chocolate: $0.75

d. Tea flavorings:
    
    i.  Lemon: $0.25
    
    ii. Mint: $0.50

3.9.1 The program should compute the total cost of the beverage with additional taxes; taxes are 13%. The final cost, with tax included, should be rounded to the nearest cent. (Note: make sure that the amount of money billed is displayed with a dollar sign and two decimal points).
